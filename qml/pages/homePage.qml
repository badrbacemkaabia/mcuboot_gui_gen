import QtQuick 2.15
import QtQuick.Controls 2.15
import "../controls"
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.0
import Qt.labs.folderlistmodel 2.1
import Qt.labs.platform 1.0

Item {
    Rectangle {
        id: rectangle
        color: "#2c313c"
        anchors.fill: parent

        Rectangle {
            id: rectangleVisible
            color: "#1d2128"
            radius: 10
            anchors.fill: parent

            Label {
                id: labelTextName
                y: 20
                height: 25
                color: "#5c667d"
                text: qsTr("Welcome")
                anchors.left: parent.left
                anchors.right: parent.right
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.leftMargin: 9
                anchors.rightMargin: 11
                font.pointSize: 14
            }

            ScrollView {
                id: scrollView
                y: -71
                height: 403
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: labelDate.bottom
                anchors.bottom: parent.bottom
                clip: true
                anchors.rightMargin: 8
                anchors.leftMargin: 12
                anchors.bottomMargin: 8
                anchors.topMargin: 10

                CustomButton {
                    id: btnGenerate
                    x: 240
                    y: 317
                    width: 300
                    height: 37
                    text: "generate"
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                    onPressed: {
                        homeFunc.cmd()
                    }

                }

                CustomButton {
                    id: btnPath
                    x: 561
                    y: 231
                    width: 58
                    height: 33
                    Layout.preferredHeight: 40
                    Layout.maximumWidth: 200
                    Layout.preferredWidth: 250
                    Layout.fillWidth: true
                    text: "..."
                    onClicked: folderDialog.open();

                }

                TextField {
                    id: textPath
                    color: "#ffffff"
                    property color colorOnFocus: "#242831"
                    property color colorDefault: "#282c34"
                    implicitHeight: 40
                    placeholderTextColor: "#81848c"
                    implicitWidth: 300
                    QtObject {
                        id: internal
                    }
                    background: Rectangle {
                        color: internal.dynamicColor
                        radius: 10
                    }
                    property color colorMouseOver: "#2b2f38"
                    x: 240
                    y: 231
                    width: 300
                    height: 33
                    selectByMouse: true
                    selectionColor: "#ff007f"
                    placeholderText: folderDialog.folder
                    selectedTextColor: "#ffffff"
                    enabled: false


                }


                ComboBox {
                    id: selectIDE
                    x: 447
                    y: 79
                    width: 172
                    height: 40
                    model: ["Eclipse IDE", "Visual Studio 2017", "Visual Studio 2015", " Visual Studio 2012", "Custom Project"]
                    onActivated: {
                        var i=currentIndex
                        homeFunc.board(model[i])
                        }
                }

                ComboBox {
                    id: boardSelect
                    x: 447
                    y: 158
                    width: 172
                    height: 40
                    model: ["STM32F401RE Nucleo", "STM32L496ZG", "ATMEL SAMD21J18A"]

                }

                Label {
                    id: label
                    x: 161
                    y: 151
                    width: 233
                    height: 40
                    color: "#f4f4f5"
                    text: qsTr("Select your board")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                Label {
                    id: label1
                    x: 161
                    y: 86
                    width: 233
                    height: 29
                    color: "#ffffff"
                    text: qsTr("Select an IDE")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    textFormat: Text.RichText
                    topInset: 5
                }


            }
            FolderDialog {
                id: folderDialog
                currentFolder: ""
                folder: StandardPaths.LocateDirectory
                onFolderChanged: {
                    homeFunc.path(folder)
                }
            }

        }
    }

}



/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.66;height:480;width:800}D{i:13}D{i:4}
}
##^##*/
