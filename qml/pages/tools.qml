import QtQuick 2.15
import QtQuick.Controls 2.15
import "../controls"
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.0
import Qt.labs.folderlistmodel 2.1
import Qt.labs.platform 1.0

Item {
    Rectangle {
        id: rectangle
        color: "#2c313c"
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        Rectangle {
            id: rectangleVisible
            color: "#1d2128"
            radius: 10
            anchors.fill: parent

            ScrollView {
                id: scrollView
                y: -71
                height: 403
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: labelDate.bottom
                anchors.bottom: parent.bottom
                clip: true
                anchors.rightMargin: 8
                anchors.leftMargin: 12
                anchors.bottomMargin: 8
                anchors.topMargin: 10

                CustomButton {
                    id: btnGenerate
                    x: 240
                    y: 172
                    width: 371
                    height: 37
                    text: "generate application + bootloader"
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                    onPressed: {
                        homeFunc.cmd()
                    }

                }

                CustomButton {
                    id: btnPath
                    x: 553
                    y: 61
                    width: 58
                    height: 40
                    Layout.preferredHeight: 40
                    Layout.maximumWidth: 200
                    Layout.preferredWidth: 250
                    Layout.fillWidth: true
                    text: "..."
                    onClicked: folderDialog.open();

                }

                TextField {
                    id: textPath
                    color: "#ffffff"
                    hoverEnabled: true
                    property color colorOnFocus: "#242831"
                    property color colorDefault: "#282c34"
                    implicitHeight: 40
                    placeholderTextColor: "#81848c"
                    implicitWidth: 300
                    QtObject {
                        id: internal
                    }
                    background: Rectangle {
                        color: internal.dynamicColor
                        radius: 10
                    }
                    property color colorMouseOver: "#2b2f38"
                    x: 240
                    y: 61
                    width: 300
                    height: 40
                    selectByMouse: true
                    selectionColor: "#ff007f"
                    placeholderText: folderDialog.folder
                    selectedTextColor: "#ffffff"
                    enabled: false


                }

                CustomButton {
                    id: btnBinary
                    x: 553
                    y: 126
                    width: 58
                    height: 40
                    text: "..."
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                    Layout.fillWidth: true
                    Layout.maximumWidth: 200
                }

                TextField {
                    id: textBinary
                    x: 240
                    y: 126
                    width: 300
                    height: 40
                    color: "#ffffff"
                    QtObject {
                        id: internal1
                    }
                    selectedTextColor: "#ffffff"
                    property color colorMouseOver: "#2b2f38"
                    implicitHeight: 40
                    enabled: false
                    property color colorDefault: "#282c34"
                    hoverEnabled: true
                    placeholderTextColor: "#81848c"
                    selectionColor: "#ff007f"
                    background: Rectangle {
                        color: internal1.dynamicColor
                        radius: 10
                    }
                    placeholderText: folderDialog.folder
                    selectByMouse: true
                    implicitWidth: 300
                    property color colorOnFocus: "#242831"
                }

                CustomButton {
                    id: btnGenerate1
                    x: 240
                    y: 337
                    width: 371
                    height: 37
                    text: "flash"
                    onPressed: {
                        homeFunc.cmd()
                    }
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                }

                CustomButton {
                    id: btnPath1
                    x: 553
                    y: 226
                    width: 58
                    height: 40
                    text: "..."
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                    Layout.fillWidth: true
                    Layout.maximumWidth: 200
                    onClicked: folderDialog.open()
                }

                TextField {
                    id: textPath1
                    x: 240
                    y: 226
                    width: 300
                    height: 40
                    color: "#ffffff"
                    QtObject {
                        id: internal2
                    }
                    selectedTextColor: "#ffffff"
                    property color colorMouseOver: "#2b2f38"
                    implicitHeight: 40
                    enabled: false
                    property color colorDefault: "#282c34"
                    hoverEnabled: true
                    placeholderTextColor: "#81848c"
                    selectionColor: "#ff007f"
                    background: Rectangle {
                        color: internal2.dynamicColor
                        radius: 10
                    }
                    placeholderText: folderDialog.folder
                    selectByMouse: true
                    implicitWidth: 300
                    property color colorOnFocus: "#242831"
                }

                CustomButton {
                    id: btnBinary1
                    x: 553
                    y: 291
                    width: 58
                    height: 40
                    text: "..."
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 250
                    Layout.fillWidth: true
                    Layout.maximumWidth: 200
                }

                TextField {
                    id: textBinary1
                    x: 240
                    y: 291
                    width: 300
                    height: 40
                    color: "#ffffff"
                    QtObject {
                        id: internal3
                    }
                    selectedTextColor: "#ffffff"
                    property color colorMouseOver: "#2b2f38"
                    implicitHeight: 40
                    enabled: false
                    property color colorDefault: "#282c34"
                    hoverEnabled: true
                    placeholderTextColor: "#81848c"
                    selectionColor: "#ff007f"
                    background: Rectangle {
                        color: internal3.dynamicColor
                        radius: 10
                    }
                    placeholderText: folderDialog.folder
                    selectByMouse: true
                    implicitWidth: 300
                    property color colorOnFocus: "#242831"
                }



            }
            FolderDialog {
                id: folderDialog
                currentFolder: ""
                folder: StandardPaths.LocateDirectory
                onFolderChanged: {
                    homeFunc.path(folder)
                }
            }

        }
    }

}



/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.66;height:480;width:800}
}
##^##*/
