import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.0


Item {


    Rectangle {
        id: rectangle
        color: "#2c313c"
        anchors.fill: parent

        Flickable {
            id: flickable
            anchors.fill: parent
            clip: true
            Component.onCompleted: {
                openDialog.open();
            }
            FileDialog {
                id: openDialog
                selectFolder: true
                onAccepted:
                {
                    console.log("Selected folder : " + folder)
                    homeFunc.path(folder)
                }

            }



            ScrollBar.vertical: ScrollBar{}
        }
    }


    }



/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.75;height:480;width:800}
}
##^##*/
