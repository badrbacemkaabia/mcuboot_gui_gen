import QtQuick 2.15
import QtQuick.Controls 2.15
import "controls"
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import QtQuick.Timeline 1.0


Window {
    id: splashScreen
    width: 380
    height: 580
    visible: true
    color: "#00000000"
    title: qsTr("MCUboot installation")

    // Remove Title Bar
    flags: Qt.SplashScreen | Qt.FramelessWindowHint

    // Internal Functions
    QtObject{
        id: internal

        function checkLogin(username, password){
            if(username === "sofia" || password === "0000"){
                var component = Qt.createComponent("welcome.qml")
                var win = component.createObject()
                win.username = username
                win.password = password
                win.show()
                visible = false
            }
        }
    }

    Rectangle {
        id: rectangle
        color: "#2c313c"
        radius: 10
        anchors.fill: parent
        z: 1
        CustomButton {
            id: btnClose
            x: 20
            width: 30
            height: 30
            opacity: 1
            text: "X"
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.rightMargin: 15
            colorPressed: "#4891d9"
            font.family: "Segoe UI"
            colorMouseOver: "#4891d9"
            colorDefault: "#4891d9"
            font.pointSize: 10
            onClicked: splashScreen.close()
        }



        Rectangle {
            id: rectangleUsername
            height: 50
            color: "#495163"
            radius: 10
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 224
            anchors.rightMargin: 50
            anchors.leftMargin: 50

            CustomTextField {
                id: textUsername
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 8
                anchors.rightMargin: 8
                anchors.topMargin: 8
                placeholderText: "Type your name"
                anchors.leftMargin: 8
            }
        }

        Rectangle {
            id: rectanglePassword
            y: 40
            width: 540
            height: 50
            color: "#495163"
            radius: 10
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: rectangleUsername.bottom
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            anchors.topMargin: 20

            CustomTextField {
                id: textPassword
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.rightMargin: 8
                anchors.leftMargin: 10
                anchors.bottomMargin: 8
                anchors.topMargin: 8
                placeholderText: "Type your password"
                echoMode: TextInput.Password

            }
        }

        CustomButton {
            id: btnLogin
            y: 428
            height: 46
            text: "Login"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 100
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            onClicked: internal.checkLogin(textUsername.text, textPassword.text)


        }



        Label {
            id: labelWelcome
            height: 25
            color: "#5c667d"
            text: qsTr("Welcome")
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            anchors.topMargin: 33
            anchors.rightMargin: 13
            anchors.leftMargin: 7
            font.pointSize: 14
        }

        Label {
            id: labelMCUboot
            height: 45
            color: "#4891d9"
            text: qsTr("<MCUboot>")
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: labelWelcome.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            anchors.rightMargin: 12
            font.pointSize: 32
            anchors.leftMargin: 14
            anchors.topMargin: 24
        }

        CircularProgressBar {
            id: circularProgressBar
            x: 55
            y: 198
            width: 317
            height: 308
            opacity: 0
            anchors.verticalCenter: parent.verticalCenter
            value: 100
            strokeBgWidth: 4
            progressWidth: 8
            anchors.horizontalCenter: parent.horizontalCenter
            progressColor: "#4891d9"
        }

      }
    DropShadow{
        anchors.fill: rectangle
        source: rectangle
        verticalOffset: 0
        horizontalOffset: 0
        radius: 10
        color: "#40000000"
        z: 0
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                duration: 3000
                running: true
                loops: 1
                to: 3000
                from: 0
            }
        ]
        enabled: true
        startFrame: 0
        endFrame: 3000

        KeyframeGroup {
            target: circularProgressBar
            property: "value"
            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 1300
                value: 100
            }
        }

        KeyframeGroup {
            target: circularProgressBar
            property: "opacity"
            Keyframe {
                frame: 1301
                value: 1
            }

            Keyframe {
                frame: 1800
                value: 0
            }

            Keyframe {
                frame: 0
                value: 1
            }
        }

        KeyframeGroup {
            target: rectangleUsername
            property: "opacity"
            Keyframe {
                frame: 1801
                value: 0
            }

            Keyframe {
                frame: 2300
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: rectanglePassword
            property: "opacity"
            Keyframe {
                frame: 1899
                value: 0
            }

            Keyframe {
                frame: 2396
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: btnLogin
            property: "opacity"
            Keyframe {
                frame: 1996
                value: 0
            }

            Keyframe {
                frame: 2504
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: labelWelcome
            property: "opacity"
            Keyframe {
                frame: 2097
                value: 0
            }

            Keyframe {
                frame: 2652
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: labelMCUboot
            property: "opacity"
            Keyframe {
                frame: 2198
                value: 0
            }

            Keyframe {
                frame: 2796
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: btnLogin
            property: "opacity"
            Keyframe {
                frame: 2298
                value: 0
            }

            Keyframe {
                frame: 2951
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }
        }

        KeyframeGroup {
            target: rectangle
            property: "height"
            Keyframe {
                frame: 1301
                value: 360
            }

            Keyframe {
                easing.bezierCurve: [0.221,-0.00103,0.222,0.997,1,1]
                frame: 1899
                value: 560
            }

            Keyframe {
                frame: 0
                value: 360
            }
        }
    }

}



/*##^##
Designer {
    D{i:0;formeditorZoom:0.66}D{i:4}D{i:8}D{i:13}
}
##^##*/
