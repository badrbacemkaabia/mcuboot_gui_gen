# This Python file uses the following encoding: utf-8
import os
import sys


from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QObject, Slot, QUrl, Signal


class HomeFunctions(QObject):

    # Execute a cmd command
    @Slot()
    def cmd(self):
        os.system('cmd /c "py mycode.py"')

    # Open File
    @Slot(str)
    def openFile(self, filePath):
        file = open(QUrl(filePath).toLocalFile(), encoding="utf-8")
        text = file.read()
        file.close()
        print(text)
        self.readText.emit(str(text))

    # Signal Set path
    setPath = Signal(str)

    # Signal Set IDE
    setIDE = Signal(str)

    # Signal Set board
    setBoard = Signal(str)

    # Signal Set Name
    setName = Signal(str)

    # Signal Visible
    isVisible = Signal(bool)

    # Open File To Text Edit
    readText = Signal(str)

    # Text String
    textField = ""

    # Show / Hide Rectangle
    @Slot(bool)
    def showHideRectangle(self, isChecked):
        print("Is rectangle visible: ", isChecked)
        self.isVisible.emit(isChecked)

    # Folder path
    @Slot(str)
    def path(self, path):
        self.setPath.emit(path)
        print("my path is", path)

    # Select board
    @Slot(str)
    def board(self, board):
        self.setBoard.emit(board)
        print("my board is", board)

    # Select IDE
    @Slot(str)
    def showIDE(self, IDE):
        self.setIDE.emit(IDE)
        print("my IDE is", IDE)


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    home_func = HomeFunctions()
    engine.rootContext().setContextProperty('homeFunc', home_func)
    # Set App Extra Info
    app.setOrganizationName("Nadine ZRIBI")
    app.setOrganizationDomain("Graduation Project")
    engine.load(os.path.join(os.path.dirname(__file__), "qml/Login.qml"))
    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())
